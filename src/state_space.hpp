#pragma once
#include <Eigen/Dense>
#include <iostream>
using namespace Eigen;
namespace signals{
float u(float t) { return 1; }
float imp(float t) { return t==0?1e38:0; }
};
constexpr double a[6][7] = {
    {1.0 / 5, 0, 0, 0, 0, 0, 0},
    {3.0 / 40, 9.0 / 40, 0, 0, 0, 0, 0},
    {44.0 / 45, -56.0 / 15, 32.0 / 9, 0, 0, 0, 0},
    {19372.0 / 6561, -25360.0 / 2187, 64448.0 / 6561, -212.0 / 729, 0, 0, 0},
    {9017.0 / 3168, -355.0 / 33, 46732.0 / 5247, 49.0 / 176, -5103.0 / 18656, 0,
     0},
    {35.0 / 384, 0, 500.0 / 1113, 125.0 / 192, -2187.0 / 6784, 11.0 / 84, 0}};

constexpr double b[7] = {
    35.0 / 384, 0, 500.0 / 1113, 125.0 / 192, -2187.0 / 6784, 11.0 / 84, 0};
constexpr double bs[7] = {
    5179.0 / 57600, 0,	     7571.0 / 16695, 393.0 / 640, -92097.0 / 339200,
    187.0 / 2100,   1.0 / 40};

constexpr double c[7] = {0, 1.0 / 5, 3.0 / 10, 4.0 / 5, 8.0 / 9, 1, 1};
class sys {
       public:
	sys(MatrixXd Am, MatrixXd Bm, MatrixXd Cm) : A(Am), B(Bm), C(Cm) {}
	void setInitialConditions(MatrixXd s) { x = s; }
	VectorXd step(auto f) {
		double e;
		VectorXd xs, xt;
		do {
			VectorXd k1 = exec(x, f(t));
			VectorXd k2 =
			    exec(x + h * (a[0][0] * k1), f(t + c[1] * h));
			VectorXd k3 =
			    exec(x + h * (a[1][0] * k1 + a[1][1] * k2),
				 f(t + c[2] * h));
			VectorXd k4 =
			    exec(x + h * (a[2][0] * k1 + a[2][1] * k2 +
					  a[2][2] * k3),
				 f(t + c[3] * h));
			VectorXd k5 =
			    exec(x + h * (a[3][0] * k1 + a[3][1] * k2 +
					  a[3][2] * k3 + a[3][3] * k4),
				 f(t + c[4] * h));
			VectorXd k6 =
			    exec(x + h * (a[4][0] * k1 + a[4][1] * k2 +
					  a[4][2] * k3 + a[4][3] * k4 +
					  a[4][4] * k5),
				 f(t + c[5] * h));
			VectorXd k7 =
			    exec(x + h * (a[5][0] * k1 + a[5][1] * k2 +
					  a[5][2] * k3 + a[5][3] * k4 +
					  a[5][4] * k5 + a[5][5] * k6),
				 f(t + c[6] * h));
			xs = x + h * (bs[0] * k1 + bs[1] * k2 + bs[2] * k3 +
				      bs[3] * k4 + bs[4] * k5 + bs[5] * k6 +
				      bs[6] * k7);
			xt = x + h * (b[0] * k1 + b[1] * k2 + b[2] * k3 +
				      b[3] * k4 + b[4] * k5 + b[5] * k6 +
				      b[6] * k7);
			e = 0;
			for (int i = 0; i < xs.rows(); i++) {
				e += pow(xs[i] - xt[i], 2);
			}
			e /= xs.rows();
			e = sqrt(e);
			h = h * pow(0.25, 1.0 / 5) * pow(maxError / e, 1.0 / 5);
		} while (e > maxError);
		x = xt;
		t += h;
		return C * x;
	}
	VectorXd exec(VectorXd s, auto i) { return A * s + B * i; }
	double t = 0;
	double h = 0.05;  // step
	double maxError = 1e-7;
	MatrixXd x;
	MatrixXd A, B, C, D;
};

