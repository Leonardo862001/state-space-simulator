#include <Eigen/Dense>
#include <iostream>
#include "../src/state_space.hpp"
using namespace Eigen;
int main() {
	MatrixXd A(2, 2);
	VectorXd B(2);
	A << 0, 1, -1, -0.1;
	B << 1, 0;
	RowVectorXd C(2);
	C << 1, 0;
	VectorXd x0(2);
	x0 << 0, 0;
	sys pendolum(A, B, C);
	pendolum.setInitialConditions(x0);
	while (pendolum.t < 120) {
		std::cout << pendolum.t << ", " << pendolum.step(signals::u) << '\n';
	}
	return 0;
}
